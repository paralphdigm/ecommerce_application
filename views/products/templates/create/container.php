<div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
          <div class="col s12">
            <div class="row">
              <h3>Create new product</h3>
              <h6>Create new product or <a href="index.php">Go back to all products</a></h6>
              <div class="divider"></div>

              <?php

                 $products = new products();

                 $name = $_POST['name'];
                 $description = $_POST['description'];
                 $price = $_POST['price'];
                 $productcategoryID = $_POST['productcategoryID'];
                 if($_FILES['image'] != null){
                   $image = addslashes(file_get_contents($_FILES['image']['tmp_name']));
                 }

                 if ($_POST['submit']) {
                   $products->store($productcategoryID,$name,$description,$price,$image);
                 }
             ?>


             <div style="height: 50px;"></div>



             <div class="col-lg-12">
                <form method="POST" action="create.php" enctype="multipart/form-data">

                 <div class="row">

                    <div class="col-lg-3">
                       <div class="thumbnail" style="padding: 30px;">
                         <div class="row">
                           <div class="col-md-10 col-md-offset-1">
                              <img style="width: 100%;" class="img-responsive" id="imageready" src="../../img/image.png"/><br>
                           </div>
                         </div>
                       </div>
                     </div>
                     <div class="col-lg-9">
                       <div class="thumbnail" style="padding: 30px;">
                         <div class="row">
                           <div class="file-field input-field col s12">
                             <div class="btn">
                               <span>DISPLAY IMAGE</span>
                               <input type="file" name="image" onchange="readURL(this);">
                             </div>
                             <div class="file-path-wrapper">
                               <input class="file-path validate" type="text" required>
                             </div>
                           </div>
                         </div>

                         <div class="form-group">
                            <label for="productcategoryID">Product Category:</label>
                            <select name="productcategoryID" required>
                              <option value="" disabled selected>Choose Product Category</option>
                              <?php
                               $productcategories = $products->create();
                               while($obj = $productcategories->fetch_object()){
                                 echo '<option value="'.$obj->productcategoryID.'">'.$obj->productcategoryName.'</option>';
                               }
                              ?>
                            </select>
                          </div>


                         <div class="form-group">
                           <label for="name">Name:</label>
                           <input type="text" id="name" name="name" class="form-control input-lg" required>
                         </div>


                         <div class="form-group">
                             <label for="description" class="control-label"> Description:</label>
                             <textarea class="form-control" required="required" name="description" cols="50" rows="10" id="description"></textarea>
                         </div>

                          <div class="form-group">
                            <label for="price">Price:</label>
                            <input type="number" id="price" name="price" class="form-control input-lg" required>
                          </div>
                       </div>
                     </div>
                   </div>

                   <input class="btn btn-primary pull-right" name="submit" type="submit" value="Save New Category For Businesses">
                 </form>
               </div>


            </div>
          </div>
      </div>
    </div>
</div>

<div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
          <div class="col s10 offset-s1 m10 offset-m1">
            <div class="row">
              <h3>Products</h3>
              <h6>All Products</h6>
              <div class="divider"></div><div style="height: 50px;"></div>

                <div class="col s10 offset-s1 m12">
                    <table class="striped responsive-table" id ='categoriestable'>
                      <thead>
                        <tr>
                            <th data-field="Number">ID</th>
                            <th data-field="Username">Name</th>
                            <th data-field="Usertype">Description</th>
                            <th data-field="Usertype">Price</th>
                            <th data-field="Usertype">Category</th>
                            <th data-field="Action">Action</th>

                        </tr>
                      </thead>

                      <tbody>
                        <?php

                            $products = new products();
                            $result = $products->index();

                            if ($result) {
                              while($product = $result->fetch_object())
                              {
                                  echo '<tr>';
                                  echo '<td>'.$product->productID.'</td>';
                                  echo '<td>'.$product->productName.'</td>';
                                  echo '<td>'.substr($product->productDescription,0,50).'..</td>';
                                  echo '<td>'.$product->productPrice.'</td>';
                                  echo '<td>'.$product->productcategoryName.'</td>';
                                  echo '<td><form class="" action="view.php" method="get"><input type="hidden" name="id" value="'.$product->productID.'"><input type="submit" value="View/Edit" class="btn btn-primary"></form></td>';
                                  echo '</tr>';
                               }
                             }


                        ?>
                      </tbody>
                    </table>

                    <div style="height: 50px;"></div>
                    <div class="divider"></div>
                    <div style="height: 20px;"></div>
                    <a href="create.php" class="waves-effect waves-light btn pull-right">ADD NEW PRODUCT</a>
                  </div>
            </div>
          </div>
      </div>
    </div>
</div>

<h3>View/Update new product</h3>
<h6>View/Update new product or <a href="index.php">Go back to all products</a></h6>
<div class="divider"></div>
<div style="height: 50px;"></div>

<div class="col-lg-12">
  <form method="POST" action="view.php?id=<?php echo $id; ?>" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $id;?>">
    <div class="row">
        <div class="col-lg-3">
          <div class="thumbnail" style="padding: 30px;">
            <div class="row">
              <div class="col-md-12">

                 <?php
                  if($products->image == null){
                    echo '<img style="width: 100%;" class="img-responsive" id="imageready" src="../../img/image.png"/><br>';
                  }
                  else{
                    echo '<img style="width: 100%;" class="img-responsive" id="imageready" src="data:image/jpeg;base64,'.base64_encode( $products->image ).'"/><br>';
                  }
                ?>
              </div>

            </div>
            <div class="row">
              <!-- <label for="image">DISPLAY IMAGE:</label> -->
              <div class="file-field input-field col s12">
                <div class="btn">
                  <span>DISPLAY IMAGE</span>
                  <input type="file" name="image"  onchange="readURL(this);">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="hidden" required>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-lg-9">
          <div class="thumbnail" style="padding: 30px;">



            <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" id="name" name="name" class="form-control input-lg" required value="<?php echo $products->productName;?>">
            </div>

            <div class="form-group">
                <label for="description" class="control-label"> Description:</label>
                <textarea class="form-control" required="required" name="description" cols="50" rows="10" id="description"><?php echo $products->productDescription;?></textarea>
            </div>

             <div class="form-group">
               <label for="price">Price:</label>
               <input type="number" id="price" name="price" class="form-control input-lg" required value="<?php echo $products->productPrice;?>">
             </div>

             <div class="form-group">
                <label for="productcategoryID">Product Category:</label>
                <select name="productcategoryID" required>

                  <?php
                  echo'<option value="'.$products->productcategoryID.'" selected>'.$products->productcategoryName.'</option>';
                  $products = new products();
                  $productcategories = $products->getallProductCategories();
                  while($obj = $productcategories->fetch_object()){
                    echo '<option value="'.$obj->productcategoryID.'">'.$obj->productcategoryName.'</option>';
                  }
                  ?>
                </select>
              </div>


          </div>
        </div>
      </div>

      <input class="btn btn-primary pull-right" name="submit" type="submit" value="Save New Category For Businesses">
    </form>
  </div>

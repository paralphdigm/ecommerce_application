<?php
session_start();
require '../../controller/database.php';
require '../../controller/auth.php';
require '../../controller/productcategories.php';
require '../../controller/products.php';
require '../../removeerrors.php';
?>
<!doctype html>
<html lang="en">
<head>
  <?php include('../template/authheader.php');?>
</head>
<body>
 <header>
     <?php include('../template/authnavigation.php');?>

 </header>
 <main>

   <?php
      $action = 'create';
       if(isset($_SESSION['username'])){
         $auth->checkuserType($_SESSION['username'],$action);
       }
       else{
         $auth->error404();
       }
    ?>

 </main>

 <footer>
 </footer>
  <?php include('../template/javascripts.php'); ?>
  <?php include('../home/templates/cart.php');?>

  <script type="text/javascript">
    function readURL(input) {
           if (input.files && input.files[0]) {
               var reader = new FileReader();

               reader.onload = function (e) {
                   $('#imageready')
                       .attr('src', e.target.result)
                       .width(300)
                       .height(150);
               };

               reader.readAsDataURL(input.files[0]);
           }
       }
    </script>
</body>
</html>

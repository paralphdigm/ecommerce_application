<div id="page-content-wrapper">
    <div class="container-fluid">

      <div class="row">
          <div class="col s10 offset-s1 m10 offset-m1" style="background: white; padding: 10px; margin-top: 30px;">

              <h4>Orders</h4>
              <h6>View/Update Orders or <a href="index.php">Go back to all Orders</a></h6>
              <div class="divider"></div>
              <br><br>
              <?php
                $current_url = base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

                     $id = $_GET['id'];
                     $orderStatus = $_POST['orderStatus'];

                     if($_POST['submit']){
                      $order->update($id,$orderStatus);
                     }

                     echo '<form method="POST" action="view.php?id='.$id.'" enctype="multipart/form-data">';
                     $orderinfo = $order->view($id);
                       if ($orderinfo) {
                           while($obj = $orderinfo->fetch_object())
                           {
              ?>
                    <div class="thumbnail" style="padding: 30px;">
                      <h5>Order Status</h5>
                      <div class="divider"></div>
                      <div class="" style="height: 40px;"></div>

                      <div class="form-group">
                           <label for="orderStatus">Order Status:</label>
                           <select name="orderStatus" required>
                             <option value="<?php echo $obj->orderStatus;?>" disabled selected><?php echo $obj->orderStatus;?></option>
                             <option value="OnProcess">OnProcess</option>
                             <option value="Processing">Processing</option>
                             <option value="Completed">Completed</option>
                           </select>
                         </div>
                         <div class="" style="height: 40px;"></div>

                          <h5>Recepient</h5>
                          <div class="divider"></div>
                          <div class="" style="height: 40px;"></div>


                          <div class="row">
                            <div class="input-field col s6">
                                <label for="Fname" class="" placeholder="First Name">First Name:</label>
                                <input class="validate" readonly="readonly" name="Fname" type="text" id="Fname" required value="<?php echo $obj->Fname;?>">
                            </div>

                            <div class="input-field col s6">
                                <label for="Lname" class="" placeholder="Username">Last Name:</label>
                                <input class="validate" readonly="readonly" name="Lname" type="text" id="Lname" required value="<?php echo $obj->Fname;?>">
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field col s6">
                                <label for="EmailAddress" class="" placeholder="Username">Email Address:</label>
                                <input class="validate" readonly="readonly" name="EmailAddress" type="text" id="EmailAddress" required value="<?php echo $obj->EmailAddress;?>">
                            </div>

                            <div class="input-field col s6">
                                <label for="ContactNo" class="" placeholder="Username">Contact Number:</label>
                                <input class="validate" readonly="readonly" name="ContactNo" type="text" id="ContactNo" required value="<?php echo $obj->ContactNo;?>">
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field col s12">
                                <label for="Address" class="" placeholder="aboutme">Address:</label>
                                <textarea class="materialize-textarea validate" readonly="readonly" name="Address" cols="50" rows="10" id="Address" required><?php echo $obj->Address;?></textarea>
                            </div>
                          </div>
                          <input type="hidden" name="id" value="<?php echo $obj->orderID;?>">


                          <h5>Products</h5>
                          <div class="divider"></div>
                          <table class="bordered">
                            <thead>
                              <tr>
                                  <th data-field="id">ID</th>
                                  <th data-field="productname">Product Name</th>
                                  <th data-field="qty">Quantity</th>
                                  <th data-field="price">Price</th>
                              </tr>
                            </thead>

                            <tbody>
                            <?php
                                echo '<tr>';
                                echo '<td>'.$obj->orderID.'</td>';
                                echo '<td>'.substr($obj->productName,0,50).'..</td>';
                                echo '<td>'.$obj->qty.'</td>';
                                echo '<td>'.$obj->productPrice.'</td>';
                                echo '</tr>';
                                echo '</tbody>';
                                echo '</table>';
                                echo '<div class="" style="height: 40px;"></div>';
                                echo '<span class="btn blue">Total : '.$obj->orderPrice.'</span></a>';
                                }
                             }
                           ?>

                      <input class="btn btn-primary pull-right" name="submit" type="submit" value="Update">
                      </form>
                  </div>

          </div>
        </div>

    </div>
</div>

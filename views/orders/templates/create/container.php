<div id="page-content-wrapper">
    <div class="container-fluid">

      <div class="row">
          <div class="col s10 offset-s1 m10 offset-m1" style="background: white; padding: 10px; margin-top: 30px;">

              <h4>Create Order</h4>
              <h6>Create new Order or <a href="index.php">Go back to all Orders</a></h6>
              <div class="divider"></div>
              <br><br>
              <?php
                $current_url = base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
                if(isset($_SESSION["products"]))
                   {

                     $id = $_POST['id'];
                     $itemcode = $_POST['item_code'];
                     $itemqty = $_POST['item_qty'];
                     $totalPrice = $_POST['totalPrice'];
                     $Lname = $_POST['Lname'];
                     $Fname = $_POST['Fname'];
                     $ContactNo = $_POST['ContactNo'];
                     $EmailAddress = $_POST['EmailAddress'];
                     $Address = $_POST['Address'];

                     if($_POST['submit']){
                      //  echo $totalPrice;
                       $home->createOrder($id,$itemcode,$itemqty,$totalPrice);
                     }

                     echo '<form method="post" action="create.php">';
                     $getuser = $dashboard->getUser($_SESSION['username']);
                       if ($getuser) {
                           while($user = $getuser->fetch_object())
                           {
              ?>
                    <div class="thumbnail" style="padding: 30px;">

                          <h5>Recepient</h5>
                          <div class="divider"></div>
                          <div class="" style="height: 40px;"></div>


                          <div class="row">
                            <div class="input-field col s6">
                                <label for="Fname" class="" placeholder="First Name">First Name:</label>
                                <input class="validate" readonly="readonly" name="Fname" type="text" id="Fname" required value="<?php echo $user->Fname;?>">
                            </div>

                            <div class="input-field col s6">
                                <label for="Lname" class="" placeholder="Username">Last Name:</label>
                                <input class="validate" readonly="readonly" name="Lname" type="text" id="Lname" required value="<?php echo $user->Fname;?>">
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field col s6">
                                <label for="EmailAddress" class="" placeholder="Username">Email Address:</label>
                                <input class="validate" readonly="readonly" name="EmailAddress" type="text" id="EmailAddress" required value="<?php echo $user->EmailAddress;?>">
                            </div>

                            <div class="input-field col s6">
                                <label for="ContactNo" class="" placeholder="Username">Contact Number:</label>
                                <input class="validate" readonly="readonly" name="ContactNo" type="text" id="ContactNo" required value="<?php echo $user->ContactNo;?>">
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field col s12">
                                <label for="Address" class="" placeholder="aboutme">Address:</label>
                                <textarea class="materialize-textarea validate" readonly="readonly" name="Address" cols="50" rows="10" id="Address" required><?php echo $user->Address;?></textarea>
                            </div>
                          </div>
                          <input type="hidden" name="id" value="<?php echo $user->id;?>">


                          <h5>Products</h5>
                          <div class="divider"></div>
                          <table class="bordered">
                            <thead>
                              <tr>
                                  <th data-field="id">ID</th>
                                  <th data-field="productname">Product Name</th>
                                  <th data-field="qty">Quantity</th>
                                  <th data-field="price">Price</th>
                                  <th data-field="action">Action</th>
                              </tr>
                            </thead>

                            <tbody>
                            <?php
                                }
                             }
                             $home->checkout();
                           ?>

                      <input class="btn btn-primary pull-right" name="submit" type="submit" value="Checkout">
                      </form>
                  </div>

                <?php
                  }else{
                  echo 'Your Cart is empty';
                }

                  ?>

          </div>
        </div>

    </div>
</div>

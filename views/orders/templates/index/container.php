<div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
          <div class="col s10 offset-s1 m10 offset-m1">
            <div class="row">
              <h3>All Orders</h3>
              <h6>All Orders</h6>
              <div class="divider"></div><div style="height: 50px;"></div>

                <div class="col s10 offset-s1 m12">
                    <table class="striped responsive-table" id ='categoriestable'>
                      <thead>
                        <tr>
                            <th data-field="Number">ID</th>
                            <th data-field="Username">Fullname</th>
                            <th data-field="Usertype">ContactNo</th>
                            <th data-field="Usertype">Address</th>
                            <th data-field="Usertype">Status</th>
                            <th data-field="Action">Action</th>

                        </tr>
                      </thead>

                      <tbody>
                        <?php

                            $orders = new orders();
                            $result = $orders->index($_SESSION['username']);

                            $x = 1;
                            if ($result) {
                              while($order = $result->fetch_object())
                              {
                                  echo '<tr>';
                                  echo '<td>'.$order->id.'</td>';
                                  echo '<td>'.$order->Fname.' '.$order->Lname.'</td>';
                                  echo '<td>'.$order->ContactNo.'</td>';
                                  echo '<td>'.$order->Address.'</td>';
                                  echo '<td>'.$order->orderStatus.'</td>';
                                  echo '<td><form class="" action="view.php" method="get"><input type="hidden" name="id" value="'.$order->orderID.'"><input type="submit" value="View/Edit" class="btn btn-primary"></form></td>';
                                  echo '</tr>';
                               }
                             }


                        ?>
                      </tbody>
                    </table>
                    <div style="height: 50px;"></div>
                    <div class="divider"></div>
                    <div style="height: 20px;"></div>

                  </div>
            </div>
          </div>
      </div>
    </div>
</div>

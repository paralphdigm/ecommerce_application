<?php
    session_start();
    require '../../controller/database.php';
    require '../../controller/auth.php';
    require '../../controller/dashboard.php';
    require '../../controller/home.php';
    require '../../controller/orders.php';
    require '../../removeerrors.php';

    $auth = new auth();
    $home = new home();
    $dashboard = new dashboard();
    $order = new orders();
?>
<!doctype html>
<html lang="en">
<head>
  <?php include('../template/authheader.php');?>
</head>
<body>
 <header>
     <?php include('../template/authnavigation.php');?>
 </header>
 <main>
   <?php
      $action = 'view';
        if(isset($_SESSION['username'])){
          include('templates/'.$action.'/wrapper.php');
        }
        else{
          $auth->error404();
        }
    ?>

 </main>

 <footer>
 </footer>
  <?php include('../template/javascripts.php'); ?>
  <?php include('../home/templates/cart.php');?>
</body>
</html>

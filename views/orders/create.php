<?php
    session_start();
    require '../../controller/database.php';
    require '../../controller/auth.php';
    require '../../controller/dashboard.php';
    require '../../controller/home.php';
    require '../../removeerrors.php';

    $home = new home();
    $dashboard = new dashboard();

?>
<!doctype html>
<html lang="en">
<head>
  <?php include('../template/authheader.php');?>
</head>
<body>
 <header>
     <?php include('../template/authnavigation.php');?>

 </header>
 <main>

   <?php
      $action = 'create';
       if(isset($_SESSION['username'])){
         include('templates/'.$action.'/wrapper.php');
       }
       else{
         $auth->error404();
       }
    ?>


 </main>

 <footer>
 </footer>
   <?php include('../home/templates/cart.php');?>
  <?php include('../template/javascripts.php'); ?>

</body>
</html>

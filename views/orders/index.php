<?php
session_start();
require '../../controller/database.php';
require '../../controller/auth.php';
require '../../controller/orders.php';
require '../../removeerrors.php';

?>
<!doctype html>
<html lang="en">
<head>
  <?php include('../template/authheader.php');?>
</head>
<body>
 <header>
     <?php include('../template/authnavigation.php');?>
 </header>
 <main>

   <?php
      $action = 'index';
       if(isset($_SESSION['username'])){
         include('templates/'.$action.'/wrapper.php');
       }
       else{
         $auth->error404();
       }
    ?>

 </main>

 <footer>
 </footer>
  <?php include('../template/javascripts.php'); ?>
  <?php include('../home/templates/cart.php');?>
</body>
</html>

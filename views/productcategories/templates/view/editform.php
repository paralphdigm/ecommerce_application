<h3>Create new product category</h3>
<h6>Create new product category or <a href="index.php">Go back to all product categories</a></h6>
<div class="divider"></div>
<div style="height: 50px;"></div>
<form method="POST" action="view.php?id=<?php echo $id; ?>" accept-charset="UTF-8">
  <input type="hidden" name="id" value="<?php echo $id;?>">
  <div class="form-group">
    <label for="name">Name:</label>
    <input type="text" id="name" name="name" class="form-control input-lg" required value="<?php echo $productCategory->productcategoryName;?>">
  </div>

  <div class="form-group">
      <label for="description" class="control-label"> Description:</label>
      <textarea class="form-control" required="required" name="description" cols="50" rows="10" id="description"><?php echo $productCategory->productcategoryDescription;?></textarea>
  </div>
   <input class="btn btn-primary pull-right" name="submit" type="submit" value="Update Category for businesses">
</form>

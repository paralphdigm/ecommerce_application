<div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
          <div class="col s10 offset-s1 m10 offset-m1">
            <div class="row">
              <h3>Categories for Products</h3>
              <h6>All Categories for Products</h6>
              <div class="divider"></div><div style="height: 50px;"></div>

                <div class="col s10 offset-s1 m12">
                    <table class="striped responsive-table" id ='categoriestable'>
                      <thead>
                        <tr>
                            <th data-field="Number">#</th>
                            <th data-field="Username">Name</th>
                            <th data-field="Usertype">Description</th>
                            <th data-field="Action">Action</th>

                        </tr>
                      </thead>

                      <tbody>
                        <?php

                            $productcategories = new productcategories();
                            $result = $productcategories->index();

                            $x = 1;
                            if ($result) {
                              while($productcategory = $result->fetch_object())
                              {
                                  echo '<tr>';
                                  echo '<td>'.$x++.'</td>';
                                  echo '<td>'.$productcategory->productcategoryName.'</td>';
                                  echo '<td>'.substr($productcategory->productcategoryDescription,0,50).'..</td>';
                                  echo '<td><form class="" action="view.php" method="get"><input type="hidden" name="id" value="'.$productcategory->productcategoryID.'"><input type="submit" value="View/Edit" class="btn btn-primary"></form></td>';
                                  echo '</tr>';
                               }
                             }


                        ?>
                      </tbody>
                    </table>

                    <div style="height: 50px;"></div>
                    <div class="divider"></div>
                    <div style="height: 20px;"></div>
                    <a href="create.php" class="waves-effect waves-light btn pull-right">CREATE NEW CATEGORY FOR JOB EMPLOYMENT</a>
                  </div>
            </div>
          </div>
      </div>
    </div>
</div>

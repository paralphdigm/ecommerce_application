<div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
          <div class="col s10 offset-s1 m10 offset-m1">
            <div class="row">
              <h3>Create new product category</h3>
              <h6>Create new product category or <a href="index.php">Go back to all product categories</a></h6>
              <div class="divider"></div>

              <?php
                 $name = $_POST['name'];
                 $description = $_POST['description'];

                 $productCategories = new productCategories();
                 if ($_POST['submit']) {
                   $productCategories->store($name,$description);
                 }
             ?>


             <div style="height: 50px;"></div>
              <form method="POST" action="create.php" accept-charset="UTF-8">
                <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text" id="name" name="name" class="form-control input-lg" required>
                </div>

                 <div class="form-group">
                     <label for="description" class="control-label"> Description:</label>
                     <textarea class="form-control" required="required" name="description" cols="50" rows="10" id="description"></textarea>
                 </div>
                 <input class="btn btn-primary pull-right" name="submit" type="submit" value="Save New Category For Businesses">
             </form>
            </div>
          </div>
      </div>
    </div>
</div>

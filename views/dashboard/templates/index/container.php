<?php

    $id = $_POST['id'];
    $Lname = $_POST['Lname'];
    $Fname = $_POST['Fname'];
    $Gender = $_POST['Gender'];
    $Birthdate = $_POST['Birthdate'];
    $ContactNo = $_POST['ContactNo'];
    $EmailAddress = $_POST['EmailAddress'];
    $address = $_POST['Address'];
    if($_FILES['image'] != null){
      $image = addslashes(file_get_contents($_FILES['image']['tmp_name']));
    }
    $dashboard = new dashboard();
    if($_POST['submit']){
      $dashboard->updateprofile($id,$Lname,$Fname,$Gender,$Birthdate,$ContactNo,$EmailAddress,$address,$image);
    }
    $getuser = $dashboard->getUser($_SESSION['username']);
      if ($getuser) {
          while($obj = $getuser->fetch_object())
          {

?>

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
              <div class="row">

                  <h1>PROFILE</h1>
                  <h6>Update your profile</h6>


              </div>
              <div class="divider"></div>
              <div style="height: 50px;"></div>

                <div class="col-lg-12">
                  <form role="form" method="post" action="index.php" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-lg-3">
                          <div class="thumbnail" style="padding: 30px;">
                            <div class="row">
                              <div class="col-md-10 col-md-offset-1">

                                 <?php
                                  if($obj->image == null)
                                      echo '<img style="width: 100%;" id ="imageready" class="img-responsive" src="../../img/avatar.png"/><br>';
                                  else
                                      echo '<img style="width: 80%;" id="imageready" class="img-responsive" src="data:image/jpeg;base64,'.base64_encode( $obj->image ).'"/><br>';
                                  ?>
                              </div>

                            </div>

                            <div class="row">
                              <!-- <label for="image">DISPLAY IMAGE:</label> -->
                              <div class="file-field input-field col s12">
                                <div class="btn">
                                  <span>DISPLAY IMAGE</span>
                                  <input type="file" name="image" onchange="readURL(this);">
                                </div>
                                <div class="file-path-wrapper">
                                  <input class="file-path validate" type="hidden" required>
                                </div>
                              </div>

                            </div>

                          </div>
                        </div>

                      <div class="col-lg-9">



                        <div class="thumbnail" style="padding: 30px;">
                          <input type="hidden" name="id" value="<?php echo $obj->id;?>">
                          <div class="row">
                            <div class="input-field">
                              <label for="Fname" class="" placeholder="First Name">First Name:</label>
                              <input class="validate" name="Fname" type="text" id="Fname" required value="<?php echo $obj->Fname;?>">
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field">
                              <label for="Lname" class="" placeholder="Last Name">Last Name:</label>
                              <input class="validate" name="Lname" type="text" id="Lname" required value="<?php echo $obj->Lname;?>">
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field">
                              <label for="Birthdate" class="" placeholder="Birthdate">Birthdate:</label>
                              <input class="validate datepicker" name="Birthdate" type="date" id="Birthdate" required value="<?php echo $obj->Birthdate;?>">
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field">
                              <label for="ContactNo" class="" placeholder="Contact Number">Contact Number:</label>
                              <input class="validate" name="ContactNo" type="text" id="ContactNo" required value="<?php echo $obj->ContactNo;?>">
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field">
                              <label for="EmailAddress" class="" placeholder="Email Address">Email Address:</label>
                              <input class="validate" data-error="wrong" data-success="right" name="EmailAddress" type="email" id="EmailAddress" required value="<?php echo $obj->EmailAddress;?>">
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field">
                              <label for="Address" class="" placeholder="aboutme">Address:</label>
                              <textarea class="materialize-textarea validate" name="Address" cols="50" rows="10" id="Address" required><?php echo $obj->Address;?></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <input class="btn btn-primary pull-right" value="UPDATE" type="submit" name="submit">
                  </form>
                  </div>

                </div>


            </div>
        </div>
</div>
<?php }} ?>

<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
 <script type="text/javascript" src="js/jquery.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script> -->
<script>
$("#menu-toggle").click(function(e) {
   e.preventDefault();
   $("#wrapper").toggleClass("toggled");
});
</script>

<!-- <script src="plugins/bootstrap/js/bootstrap.min.js"></script> -->
<script src="plugins/materializecss/js/materialize.min.js"></script>
<script type="text/javascript" src="js/gallery/jquery.easing.min.js"></script>
<script type="text/javascript" src="js/gallery/jquery.mixitup.min.js"></script>
<link rel="stylesheet" href="css/gallery/normalize.css">
<link rel="stylesheet" href="css/gallery/layout.css">

<script type="text/javascript">
    $(function () {

      var filterList = {

        init: function () {

          // MixItUp plugin
          // http://mixitup.io
          $('#portfoliolist').mixitup({
            targetSelector: '.portfolio',
            filterSelector: '.filter',
            effects: ['fade'],
            easing: 'snap',
            // call the hover effect

          });

        },



      };

      // Run the show!
      filterList.init();


    });
</script>

<!-- <script src="plugins/bootstrap/js/bootstrap.min.js"></script> -->

<script>
 $(document).ready(function() {
   $('.modal-trigger').leanModal();
     $(".button-collapse").sideNav();
     $('ul.tabs').tabs();
     $('.materialboxed').materialbox();
     $('.parallax').parallax();
     $('.tooltipped').tooltip({delay: 50});
     $('select').material_select();
     $(".alert").delay(5000).fadeOut();
     $('.scrollspy').scrollSpy();
     $('.datepicker').pickadate({
       selectMonths: true, // Creates a dropdown to control month
       selectYears: 250 // Creates a dropdown of 15 years to control year
     });
 });
</script>

<div id="modal1" class="modal modal-fixed-footer">
  <div class="modal-content">

    <div class="row">


         <?php
             if(isset($_SESSION["products"])){
         ?>
         <table class="bordered">
            <thead>
              <tr>
                  <th data-field="id">ID</th>
                  <th data-field="productname">Product Name</th>
                  <th data-field="qty">Quantity</th>
                  <th data-field="price">Price</th>
                  <th data-field="action">Action</th>
              </tr>
            </thead>
               <?php
              echo '<h4>Cart<hr><br></h4>';
               $total = 0;
               foreach ($_SESSION["products"] as $cart_itm)
               {
                   echo '<tr>';
                   echo '<td>'.$cart_itm["code"].'</td>';
                   echo '<td>'.substr($cart_itm["name"],0,50).'..</td>';
                   echo '<td>'.$cart_itm["qty"].'</td>';
                   echo '<td>'.$cart_itm["price"].'</td>';
                   echo '<td><a href="index.php?removep='.$cart_itm["code"].'&return_url='.$current_url.'"><span class="glyphicon glyphicon-remove remove-itm" aria-hidden="true"></span></a></td>';
                   echo '</tr>';

                   $subtotal = ($cart_itm["price"]*$cart_itm["qty"]);
                   $total = ($total + $subtotal);
               }
        ?>

            <tbody>

            </tbody>
          </table>

          <div style="height: 10px;"></div>
          <div class="row">
            <div class="col s3">
               <a href="views/orders/create.php"><span class="btn blue">CheckOut</span></a>
            </div>
            <div class="col s3">
               <a href="index.php?emptycart=1&return_url=<?php echo $current_url;?>"><span class="btn red">Empty Cart</span></a>
            </div>
            <div class="col s3">
            </div>
            <div class="col s3">
               <a><span class="btn blue"><?php echo "Total: ".$total;?></span></a>
            </div>
          </div>
        <?php
             }
             else{
               echo '<h4>Your Cart is empty<hr><br></h4>';
             }
        ?>






    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
  </div>
</div>

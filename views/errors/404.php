<style media="screen">
  .error h1{
    font-weight: bold;
    font-size: 25em;
  }
  .error h6{
    font-size: 3em;
  }
  .error h1,h6{
    text-align: center;
  }
</style>
<div class="row error center">
  <div class="col-md-10 col-md-offset-1">
    <h1>404</h1>
    <h6>CONTENT NOT FOUND</h6>
  </div>
</div>

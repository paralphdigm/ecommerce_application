<div id="navigation" class="navbar-fixed" style="margin-top: -2px;">
<!--- START NAVIGATION BAR ----->
<nav class="cyan black-text">
    <div class="container-fluid">
      <div class="nav-wrapper">
        <a href="#" data-activates="mobile-demo" class="button-collapse black-text"><i class="material-icons">menu</i></a>
        <ul class="side-nav" id="mobile-demo">
          <li>
            <div class="row">
              <div class="col s12" style="height: 50px; color:white; background-color: #263238; text-align:center;">
                <div class="col s6 offset-s3">
                  <span><b>NCFF</b></span>
                </div>
              </div>
            </div>
          </li>

          <li><a href="#">Home</a></li>


          <?php
             $auth = new auth();
             $auth->navigation();
           ?>



        </ul>
          <a href="index.php" class="brand-logo">E-COMMERCE</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="index.php">Home</a></li>

            <?php
            $cartcount = 0;
            if(isset($_SESSION["products"])){
              foreach ($_SESSION["products"] as $cart_itm){
                $cartcount += $cart_itm["qty"];;
              }
            }
            else{
              $cartcount = "Empty";
            }
            echo '<li><a class="modal-trigger" href="#modal1">Cart ('.$cartcount.')</a></li>';
            $auth->navigation();
            ?>
          </ul>
      </div>
    </div>
  </nav>
</div>

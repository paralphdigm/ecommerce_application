

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li>
                    <a href="../Dashboard/">Profile</a>
                </li>
                <?php

                  $auth = new auth();
                  $role = $auth->getuserType($_SESSION['username']);
                  if($role == 'merchant'){
                    echo '<li><a href="../Products/">Products</a></li>';
                    echo '<li><a href="../ProductCategories/">Product Categories</a></li>';
                  }
                ?>
                <li>
                    <a href="../Orders/">Orders</a>
                </li>
            </ul>
        </div>

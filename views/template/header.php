<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="plugins/materializecss/css/materialize.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="plugins/fotorama/fotorama.css">
<link rel="stylesheet" href="plugins/owl/owl.carousel.css">
<link rel="stylesheet" href="plugins/owl/owl.theme.css">
<link rel="stylesheet" href="plugins/owl/owl.transitions.css">
<link rel="stylesheet" href="css/portfolio.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<title>E-COMMERCE</title>
<style media="screen">
html, body {
  font-family: GillSans, Calibri, Trebuchet, sans-serif;
    background-color: #fff;
    color: #000;
    height: 100%;
    margin: 0;
}
section p, article{
  color:#263238;
}
h1,h2,h3,h4,h5,h6{

}
nav{

  padding-bottom: 3%;
  box-shadow: none;
}
ul.tabs .indicator{
 display: none;
 }
nav ul li a{
 text-decoration: none;
  font-weight: bold;
 color:#263238;
}
nav ul li .input-field{
  margin-top: 5%;
}
input[type="search"] {
   height: 100%;
   display: block;
   padding: 3%;
}
nav ul li a:hover{
 text-decoration: none;
 color:#263238;
  background: none;
}
nav ul li a:active{
 text-decoration: none;
 color:#263238;
}
nav .brand-logo{
 text-decoration: none;
 color:#263238;
  font-weight: bold;
}
.hero {
  /* vertically align its content */
  display: table;
  width: 100%;
  height: 580px;
  background: url(img/header.jpg) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
.hero .hero-content {
  /* vertically align inside parent element */
  background: rgba(38,50,56,0.8);
  display: table-cell;
  vertical-align: middle;
  text-align: center;
}
</style>

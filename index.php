<?php
    session_start();
    require 'controller/database.php';
    require 'controller/auth.php';
    require 'controller/products.php';
    require 'controller/home.php';
    require 'removeerrors.php';
?>
<!doctype html>
<html lang="en">
<head>
  <?php include('views/template/header.php');?>
</head>
<body>
 <header>
     <?php include('views/template/navigation.php');?>
 </header>

 <main>
   <section class="hero">
      <div class="hero-content">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-md-lg-6"><h1 class="bold white-text" style="font-size: 10em;">Products</h1>
          <div class="col-xs-12 col-sm-12 col-md-6 col-md-lg-6"></div>
        </div>
      </div>
    </section>
    <div class="container">
      <?php include('views/home/templates/products.php');?>
    </div>

    <?php include('views/home/templates/cart.php');?>
 </main>
 <?php include('views/home/templates/javascripts.php');?>

</body>
</html>

<?php
session_start();
if(!isset($_SESSION['username'])){
  echo '<center><br><br><h1>ACCESS DENIED</h1><br></center>';
  session_destroy();
  header("Refresh:0; url=login.php");
}
else{
  $expire = time()-86400;
  setcookie('c9', $_SESSION['username'], $expire);
  session_destroy();
  header("Refresh:0; url=login.php");
}
  ?>

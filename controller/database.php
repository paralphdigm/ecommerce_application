<?php

    function database() {
      $dbhost="localhost";
      $dbuser="root";
      $dbpass="";
      $dbname="ecommerce";
      $port = '3306';
      $dbConnection = mysqli_connect($dbhost, $dbuser, $dbpass ,$dbname) or die("We couldn't connect!");
      return $dbConnection;
    }
    function currentdate(){
    	$date = date("m.d.y");
    	$year = date("y");
    	$year = $year % 100;
    	$month = date("m");
    	$day = date("d");
    	$currentdate = date("y-m-d H:i:s");

    	return $currentdate;
    }

?>

<?php

class home{

  public function index(){

    $con = database();
    $currentdate = currentdate();
    $query = "SELECT A.*,B.productcategoryName,B.productcategoryID FROM products A,product_categories B WHERE A.productcategoryID = B.productcategoryID";
    $result = mysqli_query($con,$query);
    return $result;

  }

  public function cart(){

    $con = database();
    if(isset($_GET["emptycart"]) && $_GET["emptycart"]==1)
    {
    	$return_url = base64_decode($_GET["return_url"]);
      unset($_SESSION['products']);
    	header('Location:'.$return_url);
    }

    if(isset($_POST["type"]) && $_POST["type"]=='add')
    {
    	$product_code 	= filter_var($_POST["product_code"], FILTER_SANITIZE_STRING);
    	$product_qty 	= filter_var($_POST["product_qty"], FILTER_SANITIZE_NUMBER_INT);
    	$return_url 	= base64_decode($_POST["return_url"]);

      $query = "SELECT A.*,B.productcategoryName,B.productcategoryID FROM products A,product_categories B WHERE A.productcategoryID = B.productcategoryID and A.productID=$product_code LIMIT 1";
      $results = mysqli_query($con,$query);
      $obj = $results->fetch_object();

    	if ($results) {
    		$new_product = array(array('name'=>$obj->productName, 'code'=>$product_code, 'qty'=>$product_qty, 'price'=>$obj->productPrice));
    		if(isset($_SESSION["products"]))
    		{
    			$found = false;

    			foreach ($_SESSION["products"] as $cart_itm)
    			{
    				if($cart_itm["code"] == $product_code){

    					$product[] = array('name'=>$cart_itm["name"], 'code'=>$cart_itm["code"], 'qty'=>$product_qty, 'price'=>$cart_itm["price"]);
    					$found = true;
    				}else{
    					$product[] = array('name'=>$cart_itm["name"], 'code'=>$cart_itm["code"], 'qty'=>$cart_itm["qty"], 'price'=>$cart_itm["price"]);
    				}
    			}
    			if($found == false){
    				$_SESSION["products"] = array_merge($product, $new_product);
    			}
          else{
    				$_SESSION["products"] = $product;
    			}
    		}
        else{
    			$_SESSION["products"] = $new_product;
    		}
    	}

    	header('Location:'.$return_url);
    }

    if(isset($_GET["removep"]) && isset($_GET["return_url"]) && isset($_SESSION["products"]))
    {
    	$product_code 	= $_GET["removep"];
    	$return_url 	= base64_decode($_GET["return_url"]);

    	foreach ($_SESSION["products"] as $cart_itm)
    	{
    		if($cart_itm["code"]!=$product_code){
    			$product[] = array('name'=>$cart_itm["name"], 'code'=>$cart_itm["code"], 'qty'=>$cart_itm["qty"], 'price'=>$cart_itm["price"]);
    		}
    		$_SESSION["products"] = $product;
    	}
    	header('Location:'.$return_url);
    }
  }

  public function checkout(){

    $con = database();
    $total = 0;
    $cart_items = 0;
    if($_SESSION["products"]  != null){
      foreach ($_SESSION["products"] as $cart_itm)
          {
             $product_code = $cart_itm["code"];
             $query = "SELECT A.*,B.productcategoryName,B.productcategoryID FROM products A,product_categories B WHERE A.productcategoryID = B.productcategoryID and A.productID='$product_code' LIMIT 1";
             $results = mysqli_query($con,$query);
             $obj = $results->fetch_object();

             echo '<tr>';
             echo '<td>'.$product_code.'</td>';
             echo '<td>'.substr($obj->productName,0,50).'..</td>';
             echo '<td>'.$cart_itm["qty"].'</td>';
             echo '<td>'.$cart_itm["price"].'</td>';
             echo '<td><a href="index.php?removep='.$product_code.'&return_url='.$current_url.'"><span class="glyphicon glyphicon-remove remove-itm" aria-hidden="true"></span></a></td>';
             echo '</tr>';

            $subtotal = ($cart_itm["price"]*$cart_itm["qty"]);
            $total = ($total + $subtotal);

            echo '<input type="hidden" name="item_name['.$cart_items.']" value="'.$obj->productName.'" />';
            echo '<input type="hidden" name="item_code['.$cart_items.']" value="'.$obj->productID.'" />';
            echo '<input type="hidden" name="item_desc['.$cart_items.']" value="'.$obj->productDescription.'" />';
            echo '<input type="hidden" name="item_qty['.$cart_items.']" value="'.$cart_itm["qty"].'" />';
            $cart_items ++;

          }
          echo '</tbody>';
          echo '</table>';
          echo '<div class="" style="height: 40px;"></div>';
          echo '<span class="check-out-txt">';
          echo '<input type="hidden" name="totalPrice" value="'.$total.'" />';
          echo '<span class="btn blue">Total : '.$total.'</span></a>';

          echo '</span>';
    }


  }

  public function createOrder($id,$itemcode,$itemqty,$totalPrice){
      $con = database();
      $currentdate = currentdate();
      $orderStatus = "OnProcess";
      $query = "INSERT INTO `orders` (userID,orderStatus,orderPrice,created_at, updated_at) VALUES ('$id','$orderStatus','$totalPrice','$currentdate','$currentdate')";
      $result = mysqli_query($con,$query);

      if($result){
        $selectlastid = "SELECT * FROM orders ORDER BY orderID DESC LIMIT 0 , 1";
        $lastid = mysqli_query($con,$selectlastid);
        $lastinfo = mysqli_fetch_array($lastid, MYSQLI_ASSOC);
        $orderID = $lastinfo["orderID"];

        foreach($itemcode as $product){
          foreach($itemqty as $qty){}
          $query2 = "INSERT INTO `product_orders` (productID,orderID,qty,created_at, updated_at) VALUES ('$product','$orderID','$qty','$currentdate','$currentdate')";
          $result2 = mysqli_query($con,$query2);

          if($result2){

            unset($_SESSION['products']);
            echo '<div class="alert alert-success alert-dismissable">';
              echo '<a class="panel-close close" data-dismiss="alert">×</a> ';
                 echo '<center><b>Successfully Ordered product/s!</b></center>';
            echo '</div>';
          }
        }

      }
      else{

      }
  }
}


?>

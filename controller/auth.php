<?php

    class auth{
        public function login($username,$password){

            if($username && $password){

              $con = database();
              $query = "SELECT * FROM users WHERE username='".$username."'";
              $result = mysqli_query($con,$query);
              $rows = mysqli_num_rows($result);

              if($rows != 0){
                while($obj = $result->fetch_object()){
                  $dbname = $obj->username;
                  $dbpassword = $obj->password;
                  $role = $obj->role;
                }
                if($username == $dbname){
                    if(md5($password) == $dbpassword){
                      if($role == "customer"){
                        $url='index.php';
                      }
                      if($role == "merchant"){
                        $url='views/dashboard';
                      }
                      echo '<META HTTP-EQUIV=REFRESH CONTENT="0; '.$url.'">';
                    }
                    else{
                      echo '<div class="alert alert-danger alert-dismissable">';
                          echo '<center><b>Sorry, but your PASSWORD is incorrect.</b></center>';
                      echo '</div>';
                    }
                }
                else{

                    echo '<div class="alert alert-danger alert-dismissable">';
                        echo '<center><b>Invalid Username</b></center>';
                    echo '</div>';
                }
              }
              else{
                echo '<div class="alert alert-danger alert-dismissable">';
                    echo '<center><b>Sorry, but your USERNAME is unregistered.</b></center>';
                echo '</div>';
              }
            }
        }
        public function register($username,$password,$Lname,$Fname,$Birthdate,$ContactNo,$EmailAddress,$address,$role){

            $con = database();
            $currentdate = date("y-m-d H:i:s");
            $image = null;
            if ($Lname && $Fname && $Birthdate && $ContactNo && $EmailAddress && $username && $password && $address && $role){

                  if (preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $EmailAddress)) {

                      if (strlen($password) > 3) {

                           $usernameChecker = $this->checkusername($username);
                           $emailChecker = $this->checkemailaddress($EmailAddress);

                            if($usernameChecker != 0 || $emailChecker != 0){
                                if($usernameChecker != 0){
                                    echo '<div class="alert alert-danger alert-dismissable">';
                                      echo '<a class="panel-close close" data-dismiss="alert">×</a> ';
                                      echo "Username already exist! Please enter another user.";
                                    echo '</div>';
                                }
                                else if($emailChecker != 0){
                                      echo '<div class="alert alert-danger alert-dismissable">';
                                        echo '<a class="panel-close close" data-dismiss="alert">×</a> ';
                                        echo"Email Address already exist! Please enter another email address.";
                                      echo '</div>';
                                }
                             }
                             else{
                                  $encryptedPassword = md5($password);
                                  $insertuser = "INSERT INTO `users` (username,password,Lname,Fname,Birthdate,ContactNo,EmailAddress,Address,role,image,created_at, updated_at) VALUES ('$username', '$encryptedPassword','$Lname','$Fname','$Birthdate','$ContactNo','$EmailAddress','$address','$role','$image','$currentdate','$currentdate')";
                                  $insertuserresult = mysqli_query($con,$insertuser);

                                  if($insertuserresult){
                                       echo '<div class="alert alert-success alert-dismissable">';
                                            echo '<center><b>Congratulations you have successfully registered an account!</b></center>';
                                       echo '</div>';
                                  }
                             }
                      }
                      else{
                        echo '<div class="alert alert-danger alert-dismissable">';
                            echo '<center><b>Sorry, but your Email Address is invalid.</b></center>';
                        echo '</div>';
                      }
                 }
            }
        }
        public function logout($username){
            session_start();
            if(!isset($username)){
              echo '<center><br><br><h1>ACCESS DENIED</h1><br></center>';
              session_destroy();
              header("Refresh:0; url=logout.php");
            }
            else{
              $expire = time()-86400;
              setcookie('c9', $username, $expire);
              session_destroy();
              header("Refresh:0; url=logout.php");
            }
        }
        public function checkusername($username){
    					$con = database();
    					$usernamechecker = mysqli_query($con,"SELECT username FROM users WHERE username ='$username' ");
    					$count = mysqli_num_rows($usernamechecker);
    					return $count;
  			}

  			public function checkemailaddress($EmailAddress){

    					$con = database();
    					$emailchecker = mysqli_query($con ,"SELECT EmailAddress FROM users WHERE EmailAddress ='$EmailAddress' ");
    					$count = mysqli_num_rows($emailchecker);
    					return $count;

  			}
        public function checkuserType($username,$action){

    					$con = database();
    					$result = mysqli_query($con ,"SELECT role FROM users WHERE username ='$username' ");

              while($user = $result->fetch_object()){
                $role = $user->role;
              }

              if($role == 'customer'){
                $this->error404();
              }
              else{
                include('templates/'.$action.'/wrapper.php');
              }

  			}
        public function getuserType($username){

              $con = database();
              $result = mysqli_query($con ,"SELECT role FROM users WHERE username ='$username' ");

              while($user = $result->fetch_object()){
                $role = $user->role;
              }
              return $role;

        }
        public function navigation(){
            if(!isset($_SESSION['username'])){
               echo '<li><a href="login.php">Login</a></li>';
               echo '<li><a href="signup.php">Register</a></li>';
            }
            else{
              echo '<li><a href="views/dashboard/index.php" class="red-text">'.$_SESSION['username'].'</a></li>';
              echo '<li><a href="logout.php">Logout</a></li>';
            }
        }
        public function authnavigation(){
            if(!isset($_SESSION['username'])){
               echo '<li><a href="../../login.php">Login</a></li>';
               echo '<li><a href="../../signup.php">Register</a></li>';
            }
            else{
              echo '<li><a href="../dashboard/index.php" class="red-text">'.$_SESSION['username'].'</a></li>';
              echo '<li><a href="../../logout.php">Logout</a></li>';
            }
        }
        public function error404(){
            include('../errors/404.php');
        }


    }


?>

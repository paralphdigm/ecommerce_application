<?php


  class products{
    public function __construct(){

      // if(!isset($_SESSION['username'])){
      //   $url='../../login.php';
      //   echo '<META HTTP-EQUIV=REFRESH CONTENT="0; '.$url.'">';
      // }

    }
    public function index(){

      $con = database();
      $currentdate = currentdate();
      $query = "SELECT A.*,B.productcategoryName,B.productcategoryID FROM products A,product_categories B WHERE A.productcategoryID = B.productcategoryID";
      $result = mysqli_query($con,$query);
      return $result;

    }
    public function create($name,$description){
        $productcategories = $this->getallProductCategories();
        return $productcategories;
    }
    public function store($productcategoryID,$name,$description,$price,$image){

        $con = database();
        $currentdate = currentdate();
        $query = "INSERT INTO `products` (productcategoryID,productName,productDescription,productPrice,image,created_at,updated_at) VALUES ('$productcategoryID','$name','$description','$price','$image','$currentdate','$currentdate')";
        $result = mysqli_query($con,$query);

        if($result){
          echo '<div class="alert alert-success alert-dismissable">';
            echo '<a class="panel-close close" data-dismiss="alert">×</a> ';
               echo '<center><b>Successfully added a product!</b></center>';
          echo '</div>';
        }
        else{

        }

    }
    public function edit($id){

        $con = database();
        $currentdate = currentdate();
        $query = "SELECT A.*,B.productcategoryName,B.productcategoryID FROM products A,product_categories B WHERE A.productcategoryID = B.productcategoryID and productID =$id";
        $result = mysqli_query($con,$query);
        return $result;

    }
    public function update($id,$productcategoryID,$name,$description,$price,$image){

        $con = database();
        $currentdate = currentdate();


          $query = "UPDATE `products` SET productcategoryID = '$productcategoryID',productName = '$name',productDescription = '$description',productPrice = '$price',image = '$image',updated_at = '$currentdate' WHERE productID = $id";
          $result = mysqli_query($con,$query);

          if($result){
            echo '<div class="alert alert-success alert-dismissable">';
              echo '<a class="panel-close close" data-dismiss="alert">×</a> ';
                 echo '<center><b>Successfully updated a product!</b></center>';
            echo '</div>';
          }
          else{

          }


    }
    public function getallProductCategories(){
      $con = database();
      $currentdate = currentdate();
      $query = "SELECT * FROM product_categories";
      $result = mysqli_query($con,$query);
      return $result;
    }

  }


?>

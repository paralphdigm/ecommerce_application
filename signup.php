<?php

require 'controller/database.php';
require 'controller/auth.php';
require 'removeerrors.php';

?>
<!doctype html>
<html lang="en">
<head>
  <?php include('views/template/header.php');?>
</head>
<body>
 <header>
     <?php include('views/template/navigation.php');?>
 </header>
 <main>
   <section class="hero">
     <div class="hero-content">
       <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-6 col-lg-10 col-lg-offset-1">

           <section style="background: white; padding: 10px; margin-top: 50px;">
             <div class="spacer"></div>
                <div class="row content">
                 <div class="col-lg-12">

                   <div class="col s12">
                        <h1>SIGN UP FORM</h1>
                         <div class="row">

                           <?php
                           
                             $username = $_POST['username'];
                             $password = $_POST['password'];
                             $Lname = $_POST['Lname'];
                             $Fname = $_POST['Fname'];
                             $Birthdate = $_POST['Birthdate'];
                             $ContactNo = $_POST['ContactNo'];
                             $EmailAddress = $_POST['EmailAddress'];
                             $address = $_POST['Address'];
                             $role = $_POST['role'];

                             $auth = new auth();
                             if($_POST['submit']){
                               $auth->register($username,$password,$Lname,$Fname,$Birthdate,$ContactNo,$EmailAddress,$address,$role);
                             }

                         ?>


                           <form class="col s12" role="form" method="post" action="signup.php" enctype="multipart/form-data">
                             <div class="row">
                               <div class="input-field col s12">
                                 <select name="role" required>
                                   <option value="" disabled selected>Choose Usertype</option>
                                   <option value="customer">Customer</option>
                                   <option value="merchant">Merchant</option>
                                 </select>
                                 <label>Usertype</label>
                               </div>
                             </div>
                             <div class="row">
                               <div class="input-field col s6">
                                   <label for="username" class="" placeholder="Username">Username:</label>
                                   <input class="validate" name="username" type="text" id="username" required>
                               </div>

                               <div class="input-field col s6">
                                   <label for="password" class="" placeholder="Password">Password:</label>
                                   <input class="validate" name="password" type="password" id="password" required>
                               </div>
                             </div>

                             <div class="row">
                               <div class="input-field col s6">
                                   <label for="Fname" class="" placeholder="First Name">First Name:</label>
                                   <input class="validate" name="Fname" type="text" id="Fname" required>
                               </div>
                               <div class="input-field col s6">
                                   <label for="Lname" class="" placeholder="Last Name">Last Name:</label>
                                   <input class="validate" name="Lname" type="text" id="Lname" required>
                               </div>

                             </div>

                             <div class="row">
                               <div class="input-field col s4">
                                   <label for="Birthdate" class="" placeholder="Birthdate">Birthdate:</label>
                                   <input class="validate datepicker" name="Birthdate" type="date" id="Birthdate" required>
                               </div>

                               <div class="input-field col s4">
                                   <label for="ContactNo" class="" placeholder="Contact Number">Contact Number:</label>
                                   <input class="validate" name="ContactNo" type="text" id="ContactNo" required>
                               </div>
                               <div class="input-field col s4">
                                   <label for="EmailAddress" class="" placeholder="Email Address">Email Address:</label>
                                   <input class="validate" data-error="wrong" data-success="right" name="EmailAddress" type="email" id="EmailAddress" required>
                               </div>
                             </div>

                             <div class="row">
                               <div class="input-field col s12">
                                   <label for="Address" class="" placeholder="aboutme">Address:</label>
                                   <textarea class="materialize-textarea validate" name="Address" cols="50" rows="10" id="Address" required></textarea>
                               </div>
                             </div>
                             <input class="btn btn-primary pull-right" type="submit" name="submit" value="SUBMIT">
                           </form>
                         </div>



                   </div>
                 </div>
               </div>
           </section>

         </div>
       </div>
     </div>
   </section>


 </main>

 <footer>
 </footer>
  <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
   <script src="plugins/materializecss/js/materialize.min.js"></script>
   <!-- <script src="plugins/bootstrap/js/bootstrap.min.js"></script> -->

   <script>
     $(document).ready(function() {
       $('.modal-trigger').leanModal();
         $(".button-collapse").sideNav();
         $('ul.tabs').tabs();
         $('.materialboxed').materialbox();
         $('.parallax').parallax();
         $('.tooltipped').tooltip({delay: 50});
         $('select').material_select();
         $(".alert").delay(5000).fadeOut();
         $('.scrollspy').scrollSpy();
         $('.datepicker').pickadate({
           selectMonths: true, // Creates a dropdown to control month
           selectYears: 250 // Creates a dropdown of 15 years to control year
         });
     });
   </script>
</body>
</html>

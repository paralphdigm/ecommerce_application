<?php
    session_start();
    require 'controller/database.php';
    require 'controller/auth.php';
    require 'removeerrors.php';
?>
<!doctype html>
<html lang="en">
<head>
  <?php include('views/template/header.php');?>
</head>
<body>
 <header>
     <?php include('views/template/navigation.php');?>
 </header>
 <main>
   <section class="hero">
     <div class="hero-content">
       <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-6 col-lg-10 col-lg-offset-1">

           <section id="focusareas">
               <div class="row center">
                 <div class="col s10 offset-s1 m10 offset-m1" style="background: white; padding: 10px; margin-top: 50px;">
                   <h2>LOGIN</h2>
                    <?php
                      $_SESSION['username'] = $_POST['username'];
                      $_SESSION['password'] = $_POST['password'];
                      
                       $auth = new auth();
                       $auth->login($_SESSION['username'],$_SESSION['password']);
                     ?>

                     <div class="row">
                         <div class="col s10 offset-s1 m8 offset-m2">
                           <div class="row">
                             <form class="col s12" role="form" method="post" action="login.php" enctype="multipart/form-data">
                               <div class="row">
                                 <div class="input-field col s12">
                                   <input id="username" name="username" type="text" class="validate">
                                   <label for="username">Username</label>
                                 </div>
                               </div>
                               <div class="row">
                                 <div class="input-field col s12">
                                   <input id="password" name="password" type="password" class="validate">
                                   <label for="password">Password</label>
                                 </div>
                               </div>
                               <input class="btn btn-primary" value="LOGIN" type="submit" >
                             </form>
                           </div>
                         </div>
                     </div>
                 </div>
             </div>
           </section><br>

         </div>
       </div>
     </div>
   </section>


 </main>

 <footer>
 </footer>
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
   <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script> -->
   <script src="plugins/materializecss/js/materialize.min.js"></script>
   <!-- <script src="plugins/bootstrap/js/bootstrap.min.js"></script> -->

   <script>
     $(document).ready(function() {
       $('.modal-trigger').leanModal();
         $(".button-collapse").sideNav();
         $('ul.tabs').tabs();
         $('.materialboxed').materialbox();
         $('.parallax').parallax();
         $('.tooltipped').tooltip({delay: 50});
         $('select').material_select();
         $(".alert").delay(5000).fadeOut();
         $('.scrollspy').scrollSpy();
         $('.datepicker').pickadate({
           selectMonths: true, // Creates a dropdown to control month
           selectYears: 250 // Creates a dropdown of 15 years to control year
         });
     });
   </script>
</body>
</html>
